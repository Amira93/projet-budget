import request from "supertest";
import {server} from "../src/server";

describe('First Test', () => {
                
        it('should add a budget and send success', async ()=>{
          await request(server)
          .post('/api')
          .send({
            "titre": "legume",
            "categorie" : "carrefour",
            "montant" : -20,
            "date" : "2020/01/22"
          })
          .expect(200)
        });

        it('should update a budget and send success', async ()=>{
          await request(server)
          .put('/api')
          .send({
            "titre": "patisserie",
            "categorie" : "boulangerie",
            "montant" : -6,
            "date" : "2021/08/08",
            "id" : 15
          })
          .expect(200)
        });

      });
    
  