import {connection} from "./connection";
import {budget} from "../entity/budget";

export class budgetRepository{

     /**
     * Method that gets all budgets from database
     * @returns {Promise<budget[]>} The budget
     */
    static async findAllBudget() 
    {
        const [rows] = await connection.query('SELECT * FROM budget');
        const bud = [];
        for (const row of rows) {
            let instance = new budget(row.id, row.titre, row.categorie, row.montant, row.date);
            bud.push(instance);
    
        }
        return bud;
    }
     
    /**
     * @param {string} categorie 
    */
    static async findByCategorie(categorie)
    {
        const [rows] = await connection.query('SELECT * FROM budget WHERE categorie = ? ' ,[categorie] );
      const bud = [];
      for (const row of rows) {
          let instance = new budget(row.id, row.titre, row.categorie, row.montant, row.date);
          bud.push(instance);
      }
      return bud;
    }


    /**
     * Find a row by his id in the budget table
     * @param {String} id 
     * @returns 
     */
    static async findById(id)
    {
        const [rows] = await connection.query("SELECT * FROM budget WHERE id = ?",[id]);
        const bud = [];
        for (const row of rows) {
            let instance = new budget(row.id, row.titre, row.categorie, row.montant, row.date)
            bud.push(instance);
        }
        return bud
    }

    static async findByDate(date){
        const [rows] = await connection.query('SELECT * FROM budget WHERE YEAR(date)=?', [date]);
    
        const bud = [];
        for(const row of rows){
            let instance = new budget(row.id, row.titre, row.categorie, row.montant, row.date);
            bud.push(instance);
        }
        return bud;
    }
     
    /**
     * Method that search budgets in db
     * @param {string} term The search term
     * @returns budget which name and/or breed match the search term
     */
    static async search(term) {
        const [rows] = await connection.query('SELECT * FROM budget WHERE MONTH(date) LIKE ?', ['%'+term+'%'])
        const bud = [];
        for(const row of rows){
            let instance = new budget(row.id, row.titre, row.categorie, row.montant, row.date );
            bud.push(instance);
        }
        return bud;
    }

     /**
     * Method that persists a budget in the database
     * @param {budget} dog The budget to persist
     */
    static async addBudget(budget) {
        const [row] = await connection.query('INSERT INTO budget (titre, categorie, montant, date) VALUES (?,?,?,?)',[budget.titre, budget.categorie, budget.montant, budget.date]);
        budget.id = row.insertId;
    }

      /**
     * update a row in the budget table
     * @param {Object} budget 
     */

    static async updateBudget(budget) {
          await connection.query('UPDATE budget SET titre=?, categorie=?, montant=?, date=? WHERE id=?',[budget.titre, budget.categorie, budget.montant, budget.date, budget.id]);
      }
     
    static async deleteBudget(id){
        await connection.query('DELETE FROM budget WHERE id=?',[id]);
    }  

    
} 