import { Router } from "express";
import { budgetRepository } from "../repository/budgetRepository";


export const budgetController = Router();

budgetController.get('/', async (req, res) => {
    try {
         let budgets;
         if(req.query.search) {
           budgets = await budgetRepository.search(req.query.search);
         } else {
               budgets = await budgetRepository.findAllBudget();
           }
        res.json(budgets);
    } catch (error) {
        res.status(500).json(error);
    }
});

budgetController.get('/:id', async (req, res) => {
    try {
        let data =  await budgetRepository.findById(req.params.id);
        res.json(data);
    } catch (error) {
        console.log(error)
         res.status(500).end();
        
    }
    
   });

   budgetController.get('/:categorie', async (req, res) => {
    try {
        let data =  await budgetRepository.findByCategorie(req.params.categorie);
        res.json(data);
    } catch (error) {
       
        
    }
    
   });

   budgetController.get('/year/:date', async(req, res) =>{
       try {
        let data= await budgetRepository.findByDate(req.params.date);
        res.json(data);
       } catch (error) {
        console.log(error)
        res.status(500).end();
           
       }
       
   })

budgetController.post('/', async (req, res) =>{
       try {
           let toAdd = req.body;
           await budgetRepository.addBudget(toAdd);
           res.json(toAdd);

       }
       catch (error){
           console.log(error)
           res.status(500).end();
       }
   });

budgetController.put('/', async (req, res) =>{
    try{
    await budgetRepository.updateBudget(req.body);
    res.end();
      }
      catch (error){
          console.log(error)
          res.status(500).end();
      }
   });

budgetController.delete('/:id', async (req, res) =>{
    await budgetRepository.deleteBudget(req.params.id);
    res.end();
})   