export class budget {
    /**
     * @param {number} id
     * @param {string} titre
     * @param {string} categorie
     * @param {bouble} montant
     * @param {date} date
     */
    id;
    titre;
    categorie;
    montant;
    date;

    constructor(id, titre, categorie, montant, date)
    {
       this.id = id;
       this.titre = titre;
       this.categorie = categorie;
       this.montant = montant;
       this.date = date;
    }
}