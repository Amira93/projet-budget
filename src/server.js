import express from 'express';
import cors from 'cors';
import { budgetController } from './controlleur/budget-controller';

export const server = express();

server.use(express.json());
server.use(cors());

server.use(budgetController);
server.use('/api/budget',budgetController);