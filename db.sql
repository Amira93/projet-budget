/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/ Gestion_Budget /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE Gestion_Budget;

DROP TABLE IF EXISTS budget;
CREATE TABLE `budget` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `titre` varchar(255) NOT NULL,
  `categorie` varchar(255) NOT NULL,
  `montant` double NOT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;

INSERT INTO budget(id,titre,categorie,montant,date) VALUES(1,'achat vetement','shooping',-143.76,'2018-05-22'),(2,'achat medicament','santé',-98.9,'2019-06-13'),(4,'achat chez decathlon','sportif',-123.66,'2021-04-06'),(5,'virement de madame Amira','virements reçu',300,'2020-12-28'),(6,'vir pole emploi','allocation chomage',467.89,'2021-04-06');